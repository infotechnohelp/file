<?php

namespace Nikolajev\File\Interfaces;

interface FileInterface
{
    public function get(): array;

    public function put();
}