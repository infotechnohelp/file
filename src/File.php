<?php

namespace Nikolajev\File;

use Nikolajev\File\File\Json;

class File
{
    public static function json(string $filePath)
    {
        return new Json($filePath);
    }
}